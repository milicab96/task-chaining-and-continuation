﻿// See https://aka.ms/new-console-template for more information
var rand = new Random();
var randNum = rand.Next(0, 10);
int[] array = new int[10];
Task<int[]> task1 = new Task<int[]>(createArray);
task1.Start();
Task<int[]> task2 = task1.ContinueWith(x => multiplyArray(x.Result, randNum));
Task<int[]> task3 = task2.ContinueWith(x => sortArray(x.Result));
Task task4 = task3.ContinueWith(x => averageArray(x.Result));
Task.WaitAll(task1, task2, task3, task4);

void averageArray(int[] result)
{
    int sum = 0;
    for (int i = 0; i < 10; i++)
    {
        sum += result[i];
    }
    Console.WriteLine("Mean value is " + sum / 10);

}


int[] sortArray(int[] result)
{
    result = result.OrderBy(x => x).ToArray();
    Console.WriteLine("Sorted array:");
    for (int i = 0; i < 10; i++)
    {
        Console.WriteLine(result[i]);
    }
    return result;
}

int[] multiplyArray(int[] result, int randNum)
{
    Console.WriteLine("Multiplied array by " + randNum + ":");
    for (int i = 0; i < 10; i++)
    {
        result[i] *= randNum;
        Console.WriteLine(result[i]);
    }
    return array;
}

int[] createArray()
{
    Console.WriteLine("Generated array:");
    for (int i = 0; i < 10; i++)
    {
        array[i] = rand.Next(0, 100);
        Console.WriteLine(array[i]);
    }
    return array;
}